module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    transform: {
        '.+\\.(css|scss)$': 'jest-css-modules-transform',
    },
}
