# Front-end application starter

## Features

- Parcel
- TypeScript
- React
- SCSS modules
- Jest
- Testing library - React
- Prettier
- GitLab CI

## Usage

Start local server with HMR

```shell
yarn start
```

Build for production

```shell
yarn build
```

Apply code style format

```shell
yarn format
```

Run all tests

```shell
yarn test

# watch all tests
yarn test:watch
```

Generate types for css modules
(see [Typed css modules](https://github.com/Quramy/typed-css-modules))

```shell
yarn type-css
```

## Docker

```shell
docker build -t frontend-starter .
docker run -p 8081:80 frontend-starter
```
