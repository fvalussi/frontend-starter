import React from 'react'
import { render, RenderResult } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { PrimaryButton } from '../../ui/components/PrimaryButton/PrimaryButton'

describe('<PrimaryButton>', () => {
    it('should ...', async () => {
        const screen: RenderResult = render(<PrimaryButton label='Click me!' />)

        expect(await screen.findByText('Click me!')).toBeVisible()
    })
})
