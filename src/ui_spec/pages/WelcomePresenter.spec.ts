import {
    WelcomePresenter,
    WelcomeView,
} from '../../ui/pages/Welcome/WelcomePresenter'
import { Greet } from '../../actions/Greet'
import { instance, mock, verify } from 'ts-mockito'

describe('WelcomePresenter', function () {
    it('should greet by the name', function () {
        const view = mock<WelcomeView>()
        const greet = new Greet()
        const presenter = new WelcomePresenter(instance(view), greet)

        presenter.start('Alice')

        verify(view.updateGreeting('Hello Alice')).called()
    })
})
