import { Greet } from '../actions/Greet'

describe('Greet', () => {
    it('should say hello', () => {
        const greet = new Greet()

        const greeting = greet.execute('Alice')

        expect(greeting).toBe('Hello Alice')
    })
})
