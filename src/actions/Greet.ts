export class Greet {
    execute(name: string): string {
        return `Hello ${name}`
    }
}
