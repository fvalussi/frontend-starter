import React, { FC, Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { lazily } from 'react-lazily'
import { Loader } from './components/Loader'

const { Welcome } = lazily(() => import('./pages/Welcome/Welcome'))
const { Exit } = lazily(() => import('./pages/Exit/Exit'))

export const Router: FC = () => (
    <BrowserRouter>
        <Switch>
            <Route path={Routes.exit}>
                <Suspense fallback={<Loader />}>
                    <Exit />
                </Suspense>
            </Route>
            <Route path={Routes.welcome}>
                <Suspense fallback={<Loader />}>
                    <Welcome name={'stranger'} />
                </Suspense>
            </Route>
        </Switch>
    </BrowserRouter>
)

export enum Routes {
    welcome = '/',
    exit = '/exit',
}
