import React, { Dispatch, FC, SetStateAction, useEffect, useState } from 'react'
import styles from './Welcome.module.scss'
import { WelcomePresenter, WelcomeView } from './WelcomePresenter'
import { Greet } from '../../../actions/Greet'
import { PrimaryButton } from '../../components/PrimaryButton/PrimaryButton'
import { Routes } from '../../Router'
import { LinkTo } from '../../components/LinkTo'

class ReactWelcomeView implements WelcomeView {
    constructor(private setGreeting: Dispatch<SetStateAction<string>>) {}

    updateGreeting(greeting: string) {
        this.setGreeting(greeting)
    }
}

type Props = {
    name: string
}

export const Welcome: FC<Props> = (props: Props) => {
    const [greeting, setGreeting] = useState('')
    const presenter: WelcomePresenter = new WelcomePresenter(
        new ReactWelcomeView(setGreeting),
        new Greet()
    )

    useEffect(() => {
        presenter.start(props.name)
    }, [])

    return (
        <div className={styles['welcome']}>
            <h1>{greeting}</h1>
            <LinkTo route={Routes.exit}>
                <PrimaryButton label='Exit' />
            </LinkTo>
        </div>
    )
}
