import { Greet } from '../../../actions/Greet'

export interface WelcomeView {
    updateGreeting(greeting: string): void
}

export class WelcomePresenter {
    constructor(private view: WelcomeView, private greet: Greet) {}

    start(name: string): void {
        const greeting = this.greet.execute(name)
        this.view.updateGreeting(greeting)
    }
}
