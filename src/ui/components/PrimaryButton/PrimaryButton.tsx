import React, { FC } from 'react'
import styles from './PrimaryButton.module.scss'

type Props = {
    label: string
}

export const PrimaryButton: FC<Props> = (props: Props) => (
    <button className={styles['primary-button']}>{props.label}</button>
)
