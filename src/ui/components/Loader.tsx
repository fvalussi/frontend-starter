import React, { FC } from 'react'

export const Loader: FC = () => <div>Loading...</div>
