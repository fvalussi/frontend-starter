import React, { FC, ReactNode } from 'react'
import { Link } from 'react-router-dom'
import { Routes } from '../Router'

interface Props {
    children: ReactNode

    route: Routes
}

export const LinkTo: FC<Props> = (props: Props) => (
    <Link to={props.route}>{props.children}</Link>
)
