import React from 'react'
import ReactDom from 'react-dom'
import { Router } from './Router'

function App() {
    return (
        <div>
            <Router />
        </div>
    )
}

ReactDom.render(<App />, document.getElementById('app'))
