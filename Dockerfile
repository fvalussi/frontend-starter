# Build stage
FROM node:14-alpine as builder

WORKDIR /app

COPY package.json yarn.lock /app/
RUN yarn install

COPY ./ /app/

RUN yarn run build

# Serve stage
FROM nginx:1.15

COPY --from=builder /app/dist/ /usr/share/nginx/html
